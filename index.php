<?php
	// Кодировка UTF-8
	header('Content-Type: text/html; charset=utf-8');
	
	// Базовый URL
	$baseUrl = dirname($_SERVER['SCRIPT_NAME']).'/';
	
	// Запрашиваемый URL
	$url = str_replace($baseUrl,'',$_SERVER['REQUEST_URI']);
	
	// Переменные для подключения CSS- и JavaScript-файлов в шаблонах
	$css = array();
	$js = array();
	
	// Маршруты
	// Основной сайт
	    if ($url=='' || $url=='index.html') require('html/index.html');
	elseif ($url=='age.html') require('html/age.html');
	elseif ($url=='development.html') require('html/development.html');
	elseif ($url=='before.html') require('html/before.html');
	elseif ($url=='after.html') require('html/after.html');
	elseif ($url=='frontpage.html') require('html/frontpage.html');
	elseif ($url=='prizes/prizes.html') require('html/prizes.html');
	elseif ($url=='rules/rules.html') require('html/rules.html');
	elseif ($url=='faq/faq.html') require('html/faq.html');
	elseif ($url=='rating/rating.html') require('html/rating.html');
	elseif ($url=='profile/profile.html') require('html/profile.html');
	elseif ($url=='test') require('html/test.html');
	// Игра
	elseif ($url=='game/frontpage.html') require('html/game/frontpage.html');
	// Остальные
	else echo 'Неправильный запрос'
?>