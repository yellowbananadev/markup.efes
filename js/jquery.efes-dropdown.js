$.widget('efes.dropdown',{
	options: {
		searchEnabled: false,
		visibleItems: 6
	},
	
	_create: function() {
		this.$container = this.element.wrap('<div class="efes-dropdown-container"></div>').parent();
		this.$field = $('<div class="efes-dropdown-field"></div>');
		this.$dropdown = $('<div class="efes-dropdown-dropdown"></div>');
		this.$searchContainer = $('<div class="efes-dropdown-search-container"></div>');
		this.$search = $('<input type="text" class="efes-dropdown-search input" placeholder="Поиск..." autocomplete="off">');
		this.$dropdownClip = $('<div class="efes-dropdown-dropdown-clip"></div>');
		this.$itemsContainer = $('<div class="efes-dropdown-items-container"></div>');
		this.$items = $('<div class="efes-dropdown-items scroll-style-other"></div>');
		this.$itemsSelector = $('<div class="efes-dropdown-items-selector"></div>');
		this.$trigger = $('<div class="efes-dropdown-trigger"></div>');
		this.items = [];
		this.itemHeight = 20;
		this.opened = false;
		this.busy = false;
		this.scrollInitialized = false;
		this.searchPerformed = false;
		this.initialized = false;
		
		var $select = this.element.addClass('efes-dropdown-select').wrap('<div class="efes-dropdown-select-container"></div>')
		  , select = $select.get(0)
		;
		
		this.$dropdown.add(this.$field).add(this.$trigger).addClass('closed');
		if ($select.hasClass('search-enabled')) this.options.searchEnabled = true;
		
		// Обработка и сбор элементов списка
		for (var i=0,k=0; i<select.length; i++) {
			if (select.options[i].disabled) {
				if (i==0)
					this.$field.append($('<div class="efes-dropdown-placeholder"></div>').text(select.options[i].text));
			} else {
				this.items[k] = $('<div class="efes-dropdown-item"></div>').data({i:i,k:k})
					.mouseenter({e:this.$itemsSelector,h:this.itemHeight}, function(e){
						e.data.e.css('top',($(this).data('k')*e.data.h)+'px');
					})
					.click({w:this}, function(e){
						var w = e.data.w
						  , select = w.element.get(0)
						;
						select.value = select.options[$(this).data('i')].value;
						w.element.trigger('change');
						w.element.trigger('click'); // Для правильной работы плагина jquery.validate.js
						w.$field.text(select.options[$(this).data('i')].text);
						w._close();
					});
				this.$items.append(this.items[k].text(select.options[i].text));
				if (select.options[i].selected) this.$field.text(select.options[i].text);
				k++;
			}
		}
		
		// Поиск
		if (this.options.searchEnabled) {
			this.$itemsContainer.addClass('search-enabled');
			this.$search.bind('keyup', {w:this}, function(e) {
				var w = e.data.w
				  , stxt = $(this).val()
				  , found = []
				  , itemsHeight
				;
				for (var i=0,k=0; i<w.items.length; i++)
					if (w.items[i].text().search(new RegExp(stxt,'i'))!=-1) {
						w.items[i].data('k',k).show();
						found.push(i);
						k++;
					} else w.items[i].hide();
				itemsHeight = found.length * w.itemHeight;
				w._fit(itemsHeight);
				if (w.scrollInitialized) {
					w.$items.data('jsp').reinitialise();
					if (itemsHeight > w.itemsMaxHeight) w.$items.find('.jspPane').width(w.$items.find('.jspContainer').width()-21);
					else w.$items.find('.jspPane').width(w.$items.find('.jspContainer').width());
				}
				w.$itemsSelector.css('top',0);
				w.searchPerformed = true;
			});
		} else this.$searchContainer.hide();
		
		// Открывание
		this.$trigger.add(this.$field).click({w:this}, function(e){
			e.data.w._open();
		});
		
		// Построение HTML
		this.$container
			.append(this.$field)
			.append(this.$dropdown
				.append(this.$dropdownClip
					.append(this.$searchContainer
						.append(this.$search))
					.append(this.$itemsContainer
						.append(this.$items
							.prepend(this.$itemsSelector)))))
			.append(this.$trigger)
		;
		
		// Чтобы вычислить высоту элемента, нужно сначала прикрепить его к DOM.
		// Интересно, что Firefox считает высоту элементов, которые еще не
		// прикреплены к DOM, а Chrome - нет.
		this.itemHeight = this.items[0].height();
		this.itemsMaxHeight = this.itemHeight * this.options.visibleItems;
		this.$trigger.mouseenter({w:this}, function(e) {
			var w = e.data.w;
			if (!w.opened && !w.busy) w.$field.addClass('trigger-entered');
		}).mouseleave({w:this}, function(e) {
			var w = e.data.w;
			if (!w.opened && !w.busy) w.$field.removeClass('trigger-entered');
		});
	},
	
	_open: function() {
		if (this.busy || this.opened) return;
		this.busy = true;
		this.$dropdown.add(this.$field).add(this.$trigger).addClass('opening');
		this._fit(this.$items.height());
		setTimeout(function(w) {
			w.$items.fadeIn(400);
			setTimeout(function(w) {
				if (!w.initialized) {
					if (w.$items.height() > w.itemsMaxHeight) {
						w.$items.height(w.itemsMaxHeight).jScrollPane();
						w.scrollInitialized = true;
					}
					w.initialized = true;
				} else {
					if (w.scrollInitialized) {
						var jsp = w.$items.data('jsp');
						jsp.reinitialise();
						jsp.scrollToY(0);
					}
				}
				if (w.scrollInitialized) w.$items.find('.jspPane').width(w.$items.find('.jspContainer').width()-21);
				w.$dropdown.add(w.$field).add(w.$trigger).removeClass('opening closed trigger-entered').addClass('opened');
				w.busy = false;
				w.opened = true;
			},400,w);
		},400,this);
		
		// Закрывание
		setTimeout(function(w){
			$(document).bind('click.efesDropdownHide', {w:w}, function(e) {
				var w = e.data.w;
				if (!$(e.target).closest(w.$dropdown).length) w._close();
			});
		},1,this);
	},
	
	_close: function() {
		if (this.busy || !this.opened) return;
		this.busy = true;
		$(document).unbind('click.efesDropdownHide');
		this.$trigger.addClass('closing');
		this.$items.fadeOut(200);
		setTimeout(function(w){
			w.$dropdownClip.height(0);
		},200,this);
		setTimeout(function(w){
			w.$dropdown.add(w.$field).addClass('closing');
		},400,this);
		setTimeout(function(w){
			w.$dropdown.add(w.$field).add(w.$trigger).addClass('closed').removeClass('closing opened');
			w.$itemsSelector.css('top',0);
			if (w.options.searchEnabled) {
				for (var i=0; i<w.items.length; i++) w.items[i].data('k',i).show();
				w.$search.val('');
			}
			w.busy = false;
			w.opened = false;
		},600,this);
	},
	
	_fit: function(height) {
		var clipHeight = this.options.searchEnabled ? this.$search.height() + 46 : 28;
		clipHeight += (height > this.itemsMaxHeight) ? this.itemsMaxHeight : height;
		this.$dropdownClip.height(clipHeight);
	}
	
});