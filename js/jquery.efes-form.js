$.widget('efes.form', {
	options: {
		successMessage: 'Данные отправлены. Ответ получен. Всё ок.',
		successHandler: null, // В контексте виджета
		rules: {},
		messages: {}
	},
	
	_create: function() {
		var form = this.element
		;
		
		// Выпадающие списки
		form.find('select.efes-dropdown').dropdown();
		
		// Сраный тултипстер
		var $tooltipHolder = form.find('input, select, textarea, .efes-checkbox-control-a, .efes-dropdown-field');
		$tooltipHolder.tooltipster({
			delay: 0,
			theme: 'efes-tooltipster-error',
			trigger: 'blah'
		});
		$tooltipHolder.each(function(i,e){
			// Для стандартных полей ввода
			if ($(e).hasClass('input')) $(e).bind('mouseenter focusin', function(){
				$(this).tooltipster('show');
			}).focusout(function(){
				$(this).tooltipster('hide');
			}).mouseleave(function(){
				if (!$(this).is(':focus')) $(this).tooltipster('hide');
			});
			// Для чекбоксов и радио-кнопок
			else if ($(e).hasClass('efes-checkbox-control-a')) $(e).closest('.efes-checkbox').mouseenter(function(){
				$(this).find('.efes-checkbox-control-a').tooltipster('show');
			}).mouseleave(function(){
				$(this).find('.efes-checkbox-control-a').tooltipster('hide');
			});
			// Для выпадающих списков
			else if ($(e).hasClass('efes-dropdown-field')) $(e).closest('.efes-dropdown-container').mouseenter(function(){
				$(this).find('.efes-dropdown-field').tooltipster('show');
			}).mouseleave(function(){
				$(this).find('.efes-dropdown-field').tooltipster('hide');
			});
			else $(e).mouseenter(function(){
				$(this).tooltipster('show');
			}).mouseleave(function(){
				$(this).tooltipster('hide');
			});
		});
		
		// Кнопка с индикатором загрузки
		var $submit = form.find('input[type="submit"]')
		  , $ladda = $('<button type="submit" data-style="zoom-out">')
		;
		if (!!$submit.prop('id')) $ladda.prop('id',$submit.prop('id'));
		if (!!$submit.prop('class')) $ladda.prop('class',$submit.prop('class'));
		if (!!$submit.prop('value')) $ladda.text($submit.prop('value'));
		$submit.prop('disabled','disabled').removeProp('type').hide();
		$ladda.addClass('ladda-button').insertAfter($submit);
		this.ladda = Ladda.create($ladda.get(0));
		
		// Сообщения валидатора
		$.extend($.validator.messages, {
			required: 'Это поле необходимо заполнить',
			remote: 'Please fix this field',
			email: 'Please enter a valid email address',
			url: 'Please enter a valid URL',
			date: 'Please enter a valid date',
			dateISO: 'Please enter a valid date (ISO)',
			number: 'Please enter a valid number',
			digits: 'Please enter only digits',
			creditcard: 'Please enter a valid credit card number',
			equalTo: 'Please enter the same value again',
			accept: 'Please enter a value with a valid extension',
			maxlength: $.validator.format("Please enter no more than {0} characters"),
			minlength: $.validator.format("Please enter at least {0} characters"),
			rangelength: $.validator.format("Please enter a value between {0} and {1} characters long"),
			range: $.validator.format("Please enter a value between {0} and {1}"),
			max: $.validator.format("Please enter a value less than or equal to {0}"),
			min: $.validator.format("Please enter a value greater than or equal to {0}")
		});
		
		// Валидация
		form.validate({
			errorClass: 'with-error',
			showErrors: function(errorMap, errorList) {
				for (var i = 0, e, t; errorList[i]; i++) {
					e = errorList[i].element;
					t = $(e);
					if (e.className.indexOf('efes-checkbox-input') != -1)
						t = $('.efes-checkbox-input[name="'+$(e).prop('name')+'"]').parent().find('.efes-checkbox-control-a');
					else if (e.className.indexOf('efes-dropdown') != -1)
						t = $(e).closest('.efes-dropdown-container').find('.efes-dropdown-field');
					t.addClass(this.settings.errorClass).tooltipster('enable').tooltipster('content',errorList[i].message);
				}
				for (var i = 0, elements = this.validElements(), t; elements[i]; i++) {
					e = elements[i];
					t = $(e);
					if (e.className.indexOf('efes-checkbox-input') != -1)
						t = $('.efes-checkbox-input[name="'+$(e).prop('name')+'"]').parent().find('.efes-checkbox-control-a');
					else if (e.className.indexOf('efes-dropdown') != -1)
						t = $(e).closest('.efes-dropdown-container').find('.efes-dropdown-field');
					t.removeClass(this.settings.errorClass).tooltipster('disable');
				}
			},
			submitHandler: function(form) {
				$(form).data('efes-form').ladda.start();
				$(form).ajaxSubmit({
					success: function(a,b,c,$form) {
						var w = $form.data('efes-form');
						w.ladda.stop();
						if (typeof w.options.successHandler == 'function') w.options.successHandler.call(w);
						else {
							$('#okPopup').remove();
							$('<div>').popup({id:'ok', content:'\
								<div style="width:350px">\
									<p class="medium font-18" style="margin-top:0;">'+ w.options.successMessage  +'</p>\
									<div data-close="popup" class="button big full-width animated" onclick="$(this).closest(\'.popup-container\').popup(\'close\')">Ok</div>\
								</div>\
							'}).popup('open').find('.popup').css('min-height','0').find('.close').hide();
						}
					},
					resetForm: true
				});
			},
			rules: this.options.rules,
			messages: this.options.messages
		});
	}
});