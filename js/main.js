(function(window, $){
	$(document).ready(function(){
	
		/**
		 * Прелоадер
		 */
		// Прелоадер?
		var loaded = localStorage.getItem('efesImagesLoaded')
		  , d = new Date()
		  , today = d.getFullYear()+ '' +d.getMonth()+ '' +d.getDate()
		;
		if (loaded && loaded == today) {
			$('body').removeClass('hidden');
			goOn();
		} else {
			window.$main = $('#main').detach();
			window.$footer = $('#footer').detach();
			var $utilityImagesContainer = $('<div id="efesUtilityImagesContainer">')
			  , utilityImages = [
				'img/preloader-plank.png',
				'img/optimized-background.jpg'
			  ]
			;
			for (var i=0; i < utilityImages.length; i++) {
				utilityImages[i] = efesBaseUrl + utilityImages[i];
				$utilityImagesContainer.append('<img src="'+ utilityImages[i] +'">');
			}
			$('body').append($utilityImagesContainer);
			$utilityImagesContainer.imagesLoaded().done(function(instance){
				$utilityImagesContainer.remove();
				var $preloaderContainer = $('<div id="efesPreloaderContainer">')
				  , $preloaderVerticalAligner = $('<div id="efesPreloaderVerticalAligner">')
				  , $preloaderOuterWrapper = $('<div id="efesPreloaderOuterWrapper">')
				  , $preloaderWrapper = $('<div id="efesPreloaderWrapper">')
				  , $preloader = $('<canvas id="efesPreloader">')
				  , $preloaderPlank = $('<img id="efesPreloaderPlank">')
				  , $preloaderText = $('<div id="efesPreloaderText">')
				  , $preloaderBottomText = $('<div id="efesPreloaderBottomText">')
				  , $preloaderImagesContainer = $('<div id="efesPreloaderImagesContainer">')
				  , images = [
					'img/main-background.jpg',
					'img/parallax-background.jpg',
					'img/repeated-waves-background.jpg',
					'img/feedback-slidedown-trigger-background.png',
					'img/footer-background.png',
					'img/frontpage-board-first.png',
					'img/frontpage-board-second.png',
					'img/harm-notice.png',
					'img/beer-cap.png',
					'img/header-leaves.png',
					'img/header-wood-background.png',
					'img/play-button.png',
					'img/wood-texture.png',
					'img/yacht.png',
					'img/icon-yellow-plus.png',
					'img/game-plank-bottom.png',
					'img/game-plank-top.png',
					'img/game-plank-left.png',
					'img/game-plank-right.png',
					'img/game-dummy.jpg'
				  ]
				  , imagesLoaded = 0
				;
				$('body').removeClass('hidden').addClass('loading');
				for (var i=0; i < images.length; i++) {
					images[i] = efesBaseUrl + images[i];
					$preloaderImagesContainer.append('<img src="'+ images[i] +'">');
				}
				// HTML
				$('body').append(
					$preloaderContainer.append(
						$preloaderVerticalAligner.append(
							$preloaderOuterWrapper.append(
								$preloaderWrapper.append(
									$preloader
								).append(
									$preloaderPlank
								).append(
									$preloaderText
								)
							).append(
								$preloaderBottomText
							)
						)
					).append(
						$preloaderImagesContainer
					)
				);
				$preloader.prop({ width: 160, height: 160 });
				(function() {
					var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
					window.requestAnimationFrame = requestAnimationFrame;
				}());
				var canvas = $preloader.get(0);
				var context = canvas.getContext('2d');
				var curPerc = 0;
				var circ = Math.PI * 2;
				var quart = Math.PI / 2;
				context.lineWidth = 20;
				context.fillStyle="#001144"
				context.strokeStyle = '#ffc607';
				context.shadowOffsetX = 0;
				context.shadowOffsetY = 0;
				window.efesPreloaderDrawSector = function(perc) {
					context.clearRect(0, 0, canvas.width, canvas.height);
					// Фоновый круг
					context.beginPath();
					context.arc(80, 80, 79, 0, circ);
					context.fill();
					// Сектор
					context.beginPath();
					context.arc(80, 80, 70, -(quart), ((circ) * perc / 100) - quart, false);
					context.stroke();
				}
				$preloaderText.text(0);
				$preloaderBottomText.text('Загрузка…');
				$preloaderPlank.prop('src', efesBaseUrl +'img/preloader-plank.png').prop('alt','');
				$preloaderImagesContainer
					.data('container', $preloaderContainer)
					.imagesLoaded()
					.progress(function(instance, image) {
						imagesLoaded++;
						var percent = Math.ceil((imagesLoaded/images.length)*100);
						efesPreloaderDrawSector(percent);
						$preloaderText.text(percent);
					})
					.done(function(instance){
						var data = $(instance.elements[0]).data()
						  , d = new Date()
						  , today = d.getFullYear()+ '' +d.getMonth()+ '' +d.getDate()
						;
						data.container.fadeOut();
						setTimeout(function(today){
							localStorage.setItem('efesImagesLoaded', today);
							$('body').prepend($footer.hide()).prepend($main.hide());
							$main.fadeIn();
							$footer.fadeIn();
							setTimeout(function() {
								$('body').removeClass('loading');
							}, 400);
							goOn();
						}, 500, today);
					})
				;
			});
			
		}
		
		/**
		 * Когда изображения загружены
		 */
		function goOn() {
			
			/***********************************************************
			 * Общее
			 **********************************************************/
			
			// Добавление промо-кодов
			$('#enterCodeInputs').data('allowAdd',true);
			$('#addCodeButton').click(function() {
				var $inputs = $('#enterCodeInputs > div > div > input')
				  , $container = $('#enterCodeInputs')
				  , max = 5
				;
				if ($inputs.length < max) {
					if ($container.data('allowAdd')) {
						$container.data('allowAdd',false);
						$container.css('height','+=36');
						var $new = $('<input>').attr('type','text').attr('name','enterCode[code]['+$inputs.length+']').attr('value','').attr('class','input block full-width').hide();
						$inputs.parent().append($new);
						$new.fadeIn();
						setTimeout(function(){
							$('#enterCodeInputs').data('allowAdd',true);
						},300);
					}
				} else alert('Нельзя добавить больше '+max+' кодов');
			});
			
			// Полосы прокрутки
			$('.scroll').jScrollPane();
			
			// Выпадающие списки
			$('select.efes-dropdown').dropdown();
			
			// Обратная связь
			window.feedbackSlidedownOpened = false;
			window.feedbackSlidedownLoaded = false;
			window.feedbackSlidedownTriggered = false;
			window.efesHeaderHeight = $('#header').height();
			$('#main').append($('<div id="slidedownOverlay"></div>').hide());
			$('#feedbackSlidedownTrigger')
				.mouseenter(function() {
					if (!feedbackSlidedownTriggered)
						$('#feedbackSlidedown').css('height','32px');
				})
				.mouseleave(function() {
					if (!feedbackSlidedownTriggered)
						$('#feedbackSlidedown').css('height','30px');
				})
				.click(function() {
					feedbackSlidedownTriggered = true;
				})
			;
			$('#feedbackSlidedown .trigger, #feedbackSlidedownTrigger').click(function() {
				var slidedown = $('#feedbackSlidedown')
				  , content = slidedown.find('.content')
				  , slidedownHeight = function() {
						return $('#feedbackSlidedown > div').eq(0).outerHeight();
					}
				  , overlay = $('#slidedownOverlay')
				  , topOverlay = $('#feedbackSlidedownTopOverlay')
				;
				if (!feedbackSlidedownOpened) {
					$('body').addClass('overlaid');
					$('#header').height(880);
					$('#feedbackSlidedownTrigger').hide();
					topOverlay.show();
					overlay.fadeIn();
					slidedown.height(slidedownHeight()).addClass('triggered').addClass('opened');
					if (!feedbackSlidedownLoaded) {
						// Показать индикатор загрузки
						content.get(0).appendChild(efesSpinner.spin().el);
						$.ajax({
							url: efesBaseUrl+'html/ajax/feedback.html',
							context: content,
							cache: false,
							success: function(data) {
								var $content = $('<div>').html(data).hide();
								$(this).append($content)
									.parents('#feedbackSlidedown').addClass('loaded');
								$content.imagesLoaded().done(function(instance){
									efesSpinner.stop();
									var $content = $(instance.elements[0]);
									$content.fadeIn();
									if (feedbackSlidedownTriggered)
										$('#feedbackSlidedown').height($content.parent().parent().outerHeight());
								});
								feedbackSlidedownLoaded = true;
							},
							error: function() {
								alert('Ошибка при загрузке');
							}
						});
					}
					// Нужно, чтобы анимация проиграла до конца
					setTimeout(function() {
						feedbackSlidedownOpened = true;
					}, 400);
					// Евент на закрывание
					setTimeout(function(){
						$(document).bind('click.efesFeedbackHide', function(e) {
							if ($(e.target).closest('#feedbackSlidedown').length) return;
							$(document).unbind('click.efesFeedbackHide');
							$('#feedbackSlidedown').height(30).removeClass('triggered');
							feedbackSlidedownTriggered = false;
							setTimeout(function(){
								$('#feedbackSlidedown').removeClass('opened');
								$('#feedbackSlidedownTopOverlay').hide();
								$('#slidedownOverlay').fadeOut(400, function() {
									$('#feedbackSlidedownTrigger').show();
									feedbackSlidedownOpened = false;
									$('#header').height(efesHeaderHeight);
									$('body').removeClass('overlaid');
								});
							}, 400);
						});
					},1);
				} else {
					$(document).unbind('click.efesFeedbackHide');
					slidedown.height(30);
					// Нужно, чтобы анимация проиграла до конца
					$('#feedbackSlidedown').removeClass('triggered');
					feedbackSlidedownTriggered = false;
					setTimeout(function(){
						$('#feedbackSlidedown').removeClass('opened');
						$('#feedbackSlidedownTopOverlay').hide();
						$('#slidedownOverlay').fadeOut(400, function() {
							$('#feedbackSlidedownTrigger').show();
							feedbackSlidedownOpened = false;
							$('#header').height(efesHeaderHeight);
							$('body').removeClass('overlaid');
						});
					}, 400);
				}
			});
			
			// URL для тестовых аякс-запросов
			$.mockjax({
				url: 'http://'+ window.location.hostname + efesBaseUrl +'test.response',
				responseStatus: 200,
				responseTime: 2000
			});
			
			
			/***********************************************************
			 * Главная страница
			 **********************************************************/
			if ($('#frontpage').length) {
				(function(window, $) {
					
					// Параллакс
					window.efesFrontpageSetParallax = function() {
						$('#main, #footer').zlayer([
							{
								layer: '#frontpageYacht',
								mass: 70,
								confine: 'y',
								force: 'pull'
							},
							{
								layer: '#frontpageBackground',
								mass: 30,
								confine: 'y'
							}
						], {reposition:false});
					}
					
					// Проигрывание анимации 1 раз за сессию
					if ($.cookie('efesFrontpageAnimationSeen')==undefined) {
						$('#main').removeClass('no-animation');
						$('#header').hide().removeClass('visible');
						setTimeout(function(){
							$('#frontpage').addClass('visible');
							$('#header').show();
							setTimeout(function(){
								$('#header').addClass('visible');
								setTimeout(function(){
									efesFrontpageSetParallax();
								}, 500);
							}, 500);
						}, 1000);
						var url = window.location.pathname;
						$.cookie('efesFrontpageAnimationSeen',1,{path:url.substring(0,url.lastIndexOf('/')+1)});
					} else {
						$('#frontpage').addClass('visible');
						efesFrontpageSetParallax();
					}
				
				})(window, $);
			}
			
			
			/***********************************************************
			 * Призы
			 **********************************************************/
			if ($('#prizesShowcase').length) {
				(function(window, $) {
				
					// Ротатор призов
					var	id = 1,
						left = $('#prizesShowcaseRotatorControlLeft'),
						right = $('#prizesShowcaseRotatorControlRight'),
						images = $('.prizes-showcase-podium-prize'),
						contents = $('.prizes-showcase-podium-content'),
						smalls = $('.prizes-showcase-plank-item')
					;
					function updateRotator() {
						left.show();
						right.show();
						if (id == 1) left.hide();
						if (id == images.length) right.hide();
						images.each(function(i,el) {
							$(el).hide();
							if ($(el).data('id')==id) $(el).show();
						});
						contents.each(function(i,el) {
							$(el).hide();
							if ($(el).data('id')==id) $(el).show();
						});
						smalls.each(function(i,el) {
							$(el).removeClass('current');
							if ($(el).data('id')==id) $(el).addClass('current');
						});
					}
					left.click(function() {
						id--;
						updateRotator();
					});
					right.click(function() {
						id++;
						updateRotator();
					});
					smalls.each(function(i,el) {
						$(el).click(function(){
							id = $(this).data('id');
							updateRotator();
						});
					});
					updateRotator();
					
				})(window, $);
			}
			
			
			/***********************************************************
			 * FAQ
			 **********************************************************/
			if ($('#faq').length) {
				(function(window, $) {
				
					var items = $('.faq-item');
					$('.faq-item-trigger').click(function() {
						if (!$(this).hasClass('opened')) {
							$('.faq-item-content, .faq-item-trigger-icon, .faq-item-trigger').each(function(i,el){
								$(el).removeClass('opened');
							});
							$(this).parents('.faq-item').find('.faq-item-content, .faq-item-trigger-icon, .faq-item-trigger').each(function(i,el){
								$(el).addClass('opened');
							});
						} else
							$(this).parents('.faq-item').find('.faq-item-content, .faq-item-trigger-icon, .faq-item-trigger').each(function(i,el){
								$(el).removeClass('opened');
							});
						var jsp = $(this).parents('.scroll').data('jsp');
						jsp.reinitialise();
						jsp.scrollToElement(this,true,true);
					});
					
				})(window, $);
			}
			
			
			/***********************************************************
			 * Выбор возраста
			 **********************************************************/
			if ($('#efesAgeForm').length) {
				(function(window, $) {
				
					$('#efesAgeForm').form({
						successHandler: function() {
							window.location = 'http://'+ window.location.hostname + efesBaseUrl +'frontpage.html';
						}
					});
					
				})(window, $);
			}
			
			
			/***********************************************************
			 * Личный кабинет
			 **********************************************************/
			if ($('#profile').length) {
				(function(window, $) {
				
					// Доска с призами
					var	id,
						smalls = $('.efes-profile-prizes-plank-item')
					;
					function updatePlank() {
						smalls.each(function(i,el) {
							$(el).removeClass('current');
							if ($(el).data('id')==id) $(el).addClass('current');
						});
					}
					smalls.each(function(i,el) {
						$(el).click(function(){
							id = $(this).data('id');
							// Попап при клике на тапочки
							if (id == 3) {
								var a = $('#efesProfileSlippersPopup');
								if (a.length) a.popup('open');
								else $('<div>').popup({id:'efesProfileSlippers',url:'http://'+window.location.hostname+efesBaseUrl+'html/ajax/profile-slippers.html'}).popup('open');
							}
							updatePlank();
						})
						.mouseenter(function() {
							var c = $(this).offset(),
								id = $(this).data('id'),
								el = $('.efes-profile-prizes-plank-item-big[data-id="'+id+'"]')
							;
							if (!el.hasClass('efes-reattached'))
								el.detach().appendTo('body').addClass('efes-reattached');
							el.css({top:(c.top-el.height()-60)+'px',left:(c.left-220+$(this).width()/2)+'px'}).show();
						}).mouseleave(function() {
							$('.efes-profile-prizes-plank-item-big[data-id="'+$(this).data('id')+'"]').hide();
						});
					});
					updatePlank();
					
					// Таблица с заказанными призами
					var tableId = 1,
						tablePrizes = $('.efes-profile-prizes-table-prize')
						tableInitialized = false
					;
					function updateTable() {
						if (!tablePrizes.length) return;
						if (!tableInitialized) {
							$('#efesProfilePrizesTablePrize')
								.addClass('center')
								.append($('<div id="efesProfilePrizesTableControlLeft" class="efes-profile-prizes-table-control"></div>'))
								.append($('<div id="efesProfilePrizesTableControlRight" class="efes-profile-prizes-table-control"></div>'))
							;
							$('#efesProfilePrizesTableControlLeft').click(function(){
								tableId-=1;
								updateTable();
							});
							$('#efesProfilePrizesTableControlRight').click(function(){
								tableId+=1;
								updateTable();
							});
							tableInitialized = true;
						}
						if (tableId == 1) $('#efesProfilePrizesTableControlLeft').hide();
						else $('#efesProfilePrizesTableControlLeft').show();
						if (tableId == tablePrizes.length) $('#efesProfilePrizesTableControlRight').hide();
						else $('#efesProfilePrizesTableControlRight').show();
						tablePrizes.hide().filter('[data-id="'+tableId+'"]').show();
						$('.efes-profile-prizes-table-date').hide().filter('[data-id="'+tableId+'"]').show();
					}
					updateTable();
					
					// Кнопка "Играть"
					$('#profilePlayButtonClickArea').mouseover(function() {
						$(this).parent().addClass('hover');
					}).mouseout(function() {
						$(this).parent().removeClass('hover');
					});
					
				})(window, $);
			}
			
			
			/***********************************************************
			 * Игра
			 **********************************************************/
			if ($('#efesGame').length) {
				(function(window, $) {
					
					window.onload = function() {
						/***************************************************
						 * Попап "Играй и займи каюту на яхте" (вариант 1)
						 **************************************************/
						$('<div>').popup({id:'play', content:'\
							<div style="width:780px; position:relative;">\
								<div id="efesGameYacht"></div>\
								<div>\
									<h1 class="font-37">Играй и займи каюту<br> на яхте</h1>\
									<p class="medium font-18" style="margin-top:0;">\
										В ходе игры тебе предстоит проплыть на яхте мимо самых<br>\
										знаменитых тусовок Средиземного моря!<br>\
										Отправляй Party Kit на остров, чтобы начать веселье!<br>\
										Твоя цель - устроить как можно больше вечеринок!\
									</p>\
									<p class="medium-13" style="margin-top:20px;">\
										Количество снарядов ограничено, как только твои снаряды заканчиваются – подводятся итоги игры.<br>\
										Баллы начисляются за каждое попадание и за каждую вечеринку на острове.<br>\
										В рейтинге фиксируется твой лучший результат.\
									</p>\
									<div style="height:170px;"></div>\
									<hr>\
									<p class="medium font-18 left">Регистрируй коды и получай дополнительные<br> преимущества в игре!</p>\
									<p class="medium-13 left">\
										<span class="yellow bold font-23 normalcase">2 кода*</span> — В два раза больше снарядов!<br>\
										<span class="yellow bold font-23 normalcase">8 кодов*</span> — Автонаведение снаряда<br>\
										<span class="yellow bold font-23 normalcase">12 кодов*</span> — В два раза больше очков!\
									</p>\
									<p class="left">\
										<span class="yellow bold font-18">*</span><span class="dark-blue normalcase"> — Общее количество зарегистрированных кодов</span>\
									</p>\
									<hr>\
									<p class="medium font-18">Набери не менее 1000 баллов и займи каюту на яхте!</p>\
									<a href="'+ efesBaseUrl +'html/game/ajax/rules.html" rel="popup" data-popup-id="rules" class="button big full-width animated">Правила акции</a>\
								</div>\
								<div id="efesGamePlayButton"><a id="profilePlayButtonClickArea" onclick="$(this).closest(\'.popup-container\').popup(\'close\')"></a></div>\
							</div>\
							<script>\
								$(\'#profilePlayButtonClickArea\').mouseover(function() {\
									$(this).parent().addClass(\'hover\');\
								}).mouseout(function() {\
									$(this).parent().removeClass(\'hover\');\
								});\
							</script>\
						'}).popup('open').find('.popup').css('min-height','0').find('.close').hide();
					}
					
					// При клике на "Сайт акции"
					// показать попап с предупреждением
					$('#navMain a').eq(0).click(function(e) {
						e.preventDefault();
						var p = $('#warningPopup');
						if (p.length) p.popup('open');
						else
							/***************************************************
							 * Попап с предупреждением о потере результата
							 **************************************************/
							$('<div>').popup({id:'warning', content:'\
								<div style="width:600px">\
									<h1>Ты уверен, что хочешь<br> покинуть игру?</h1>\
									<p class="medium font-18" style="margin-top:0;">Набранные баллы не сохранятся, если ты сейчас<br> покинешь игру</p>\
									<div class="button big full-width animated" onclick="window.location.href=\''+ $(this).prop('href') +'\'" style="margin-bottom:20px;">Да</div>\
									<div class="button big full-width animated" onclick="$(this).closest(\'.popup-container\').popup(\'close\')">Отмена</div>\
								</div>\
							'}).popup('open').find('.popup').css('min-height','0').find('.close').hide();
							
							/**************************************************************
							 * ВНИМАНИЕ!
							 * Ниже идут остальные попапы, отрисованные в дизайне.
							 **************************************************************/
							
							/***************************************************
							 * Попап "Игра окончена" (гость)
							 **************************************************/
							/*
							$('<div>').popup({id:'warning', content:'\
								<div style="width:600px">\
									<h1>Игра окончена!</h1>\
									<p class="medium font-18" style="margin-top:0;">\
										Твой результат:<br> <span id="efesGameScore" class="font-37 yellow bold italic">1250</span>\
									</p>\
									<p class="medium-13"><a href="'+ efesBaseUrl +'html/ajax/login.html" rel="popup" data-popup-id="login" class="bold italic">Авторизуйся</a>, чтобы сохранить результат игры!</p>\
									<hr>\
									<p class="medium font-18" style="margin-top:0;">Хочешь улучшить свой результат?</p>\
									<p class="medium-13" style="margin-bottom:30px;"><a href="'+ efesBaseUrl +'html/game/ajax/profile.html" rel="popup" data-popup-id="profile" class="bold italic">Регистрируй коды</a> и получай дополнительные преимущества в игре!</p>\
									<div class="button big full-width animated" onclick="$(this).closest(\'.popup-container\').popup(\'close\')">Закрыть</div>\
								</div>\
							'}).popup('open').find('.popup').css('min-height','0').find('.close').hide();
							*/
							
							/***************************************************
							 * Попап "Игра окончена" (зарегистированный)
							 **************************************************/
							/*
							$('<div>').popup({id:'warning', content:'\
								<div style="width:600px">\
									<h1>Игра окончена!</h1>\
									<p class="medium font-18" style="margin-top:0;">\
										Твой результат:<br> <span id="efesGameScore" class="font-37 yellow bold italic">1250</span>\
									</p>\
									<hr>\
									<p class="medium font-18" style="margin-top:0;">Хочешь улучшить свой результат?</p>\
									<p class="medium-13" style="margin-bottom:30px;"><a href="'+ efesBaseUrl +'html/game/ajax/profile.html" rel="popup" data-popup-id="profile" class="bold italic">Регистрируй коды</a> и получай дополнительные преимущества в игре!</p>\
									<div class="button big full-width animated" onclick="$(this).closest(\'.popup-container\').popup(\'close\')">Закрыть</div>\
								</div>\
							'}).popup('open').find('.popup').css('min-height','0').find('.close').hide();
							*/
							
							/***************************************************
							 * Попап "Играй и займи каюту на яхте" (вариант 2)
							 **************************************************/
							/*
							$('<div>').popup({id:'warning', content:'\
								<div style="width:780px;">\
									<h1 class="font-37">Играй и займи каюту<br> на яхте</h1>\
									<p class="medium font-18" style="margin-top:0;">\
										В ходе игры тебе предстоит проплыть на яхте мимо самых<br>\
										знаменитых тусовок Средиземного моря!<br>\
										Отправляй Party Kit на остров, чтобы начать веселье!<br>\
										Твоя цель - устроить как можно больше вечеринок!\
									</p>\
									<p class="medium-13" style="margin-top:20px; margin-bottom:30px;">\
										Количество снарядов ограничено, как только твои снаряды заканчиваются – подводятся итоги игры.<br>\
										Баллы начисляются за каждое попадание и за каждую вечеринку на острове.<br>\
										В рейтинге фиксируется твой лучший результат.\
									</p>\
									<div class="button big full-width animated" onclick="$(this).closest(\'.popup-container\').popup(\'close\')">Вперёд!</div>\
								</div>\
							'}).popup('open').find('.popup').css('min-height','0').find('.close').hide();
							*/
					});
					
					
					
				})(window, $);
			}
		}
		
	});
})(window, jQuery);

// Индикатор загрузки http://fgnass.github.io/spin.js/
var efesSpinner = new Spinner({
	lines: 11, // The number of lines to draw
	length: 5, // The length of each line
	width: 4, // The line thickness
	radius: 10, // The radius of the inner circle
	corners: 1, // Corner roundness (0..1)
	rotate: 0, // The rotation offset
	direction: 1, // 1: clockwise, -1: counterclockwise
	color: '#fff', // #rgb or #rrggbb or array of colors
	speed: 1, // Rounds per second
	trail: 60, // Afterglow percentage
	shadow: true, // Whether to render a shadow
	hwaccel: true, // Whether to use hardware acceleration
	className: 'spinner', // The CSS class to assign to the spinner
	zIndex: 2e9, // The z-index (defaults to 2000000000)
	top: '50%', // Top position relative to parent
	left: '50%' // Left position relative to parent
});