$.widget('efes.popup',{
	options: {
		id: 'common',
		content: 'Содержимое попапа'
	},
	
	_create: function() {
		this.shown = false;
		this.loaded = false;
		this.busy = false;
		this.content = $('<div class="content"></div>');
		this.overlay = $('#popupOverlay');
		this.closeTrigger = $('<div class="close-trigger"></div>');
		this.closeButton = $('<div class="close animated-circle-icon blue cross" title="Закрыть"><span></span></div>').click(function(){
			$(this).closest('.popup-container').popup('close');
		});
		this.element
			.prop('id', this.options.id+'Popup')
			.prop('class', 'popup-container row-table')
			.append($('<div class="popup-container-inner center middle"></div>')
				.append(this.closeTrigger)
				.append($('<div class="popup"></div>')
					.append(this.closeButton)
					.append(this.content)
				)
			)
			.hide()
		;
		this.closeTrigger.click(function(){
			$(this).parents('.popup-container').popup('close');
		});
		$('body').append(this.element);
	},
	
	open: function() {
		// Сохранение начальной высоты контента
		if (!efesPopupEverSummoned) {
			efesPopupEverSummoned = true;
			var h = $('#content').height();
			$('#content').data('height',h).height(h);
		}
		$('html, body').animate({scrollTop:0},800);
		if (!this.shown) {
			// Если есть открытый попап (метод вызван из попапа), то закрыть открытый, а затем открыть новый
			var openedPopup = $('.popup-container.opened');
			if (openedPopup.length) openedPopup.popup('close', this.options.id);
			else {
				// Если попап не занят проигрыванием анимации
				if (this.busy) return;
				this.busy = true;
				this.overlay.fadeIn();
				this.element.removeClass('closed').show();
				setTimeout(function(widget){
					widget.element.addClass('opened');
					widget.shown = true;
					widget.busy = false;
					// TODO: Поправить говнокод
					// Если контент большой, увеличить высоту контента страницы
					if (widget.content.height()-200 > $('#content').data('height')) $('#content').height(widget.content.height()-200);
					if (widget.closeTrigger.height() < $('#main').height()) widget.closeTrigger.height($('#main').height());
				},400,this);
				// Если контент еще не был загружен, то загрузить
				if (!this.loaded) {
					if (this.options.url) {
						// Показать индикатор загрузки
						this.content.get(0).appendChild(efesSpinner.spin().el);
						$.ajax({
							url: this.options.url
						  , context: this.content
						  , dataType: 'html'
						  , cache: false
						  , success: function(data) {
								// Скрыть индикатор загрузки
								efesSpinner.stop();
								$(this)
									.append($('<div>').html(data).hide().fadeIn())
									.parents('.popup-container')
									.addClass('loaded')
									.data('efes-popup').loaded = true;
									// TODO: Поправить говнокод
									// Если контент большой, увеличить высоту контента страницы
									if ($(this).height()-200 > $('#content').data('height')) $('#content').height($(this).height()-200);
									if ($(this).closest('.close-trigger').height() < $('#main').height()) $(this).closest('.close-trigger').height($('#main').height());
							}
						  , error: function() {
								alert('Ошибка при загрузке. Сообщите кому-нибудь.');
							}
						});
					} else {
						this.content.html(this.options.content);
						this.loaded = true;
					}
				}
			}
		}
	},
	
	close: function(thenOpen) {
		if (this.busy || !this.shown) return;
		this.busy = true;
		this.element.removeClass('opened').addClass('closed');
		setTimeout(function(widget,thenOpen){
			widget.element.hide();
			widget.overlay.fadeOut();
			widget.shown = false;
			widget.busy = false;
			$('#content').height($('#content').data('height'));
			if (thenOpen) $('#'+thenOpen+'Popup').popup('open');
		},400,this,thenOpen);
	}
});

var efesPopupEverSummoned = false;

$(function() {
	// Закрыть активный попап при нажатии Esc
	$(document).keyup(function(e) {
		if (e.keyCode == 27) {
			var openedPopup = $('.popup-container.opened');
			if (openedPopup.length) openedPopup.popup('close');
		}
	});

	// Добавление оверлея
	$('<div id="popupOverlay"></div>').hide().appendTo('body');

	// Привязка к ссылочкам
	$(document).on('click', 'a[rel="popup"]', function(e){
		e.preventDefault();
		var data = $(this).data()
		  , id = $.efes.popup.prototype.options.id
		  , options = {}
		  , url = $(this).prop('href')
		  , $popup
		  , widget
		;
		if (data.popupId) {
			id = data.popupId;
			options.id = data.popupId;
		}
		$popup = $('#'+id+'Popup');
		if (url)
			options.url = url;
		// Если попап с таким Id ещё не создан
		if (!$popup.length) {
			$('<div>').popup(options);
			$popup = $('#'+id+'Popup');
		}
		// Если попап уже создан, но различаются урл, то перезагрузить
		else {
			widget = $popup.data('efes-popup');
			if (widget.options.url != url) {
				widget.options.url = url;
				$popup.removeClass('loaded').find('.content').empty();
				widget.loaded = false;
			}
		}
		// Открытие попапа
		$popup.popup('open');
	});
});
